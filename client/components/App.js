import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';
import Main from './Main';

function mapStateToProps(state) {
	return {
		posts: state.posts,
		comments: state.comments
	}
}

function mapDispatchProps(dispatch) {
	return bindActionCreators(actionCreators, dispatch);
}

// adds all out state props and dispatch and actioncreators to the main component
// it's like sprinkling all the data and redux on top of our Main component
const App = connect(mapStateToProps, mapDispatchProps)(Main);

export default App;
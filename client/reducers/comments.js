function postComments(state = [], action) {
	switch (action.type) {
		case 'ADD_COMMENT':
			// return the new sate with the new comment
			return [...state, {
				user: action.author,
				text: action.comment
			}];
		case 'REMOVE_COMMENT':
			console.log('removing a comment');
			// return new state without deleted comment
			return [
				// from start to the one to delete
				...state.slice(0, action.index),
				// after deleted one (skipped between the slices), to the end
				...state.slice(action.index + 1)
			]
			return state;
		default:
			return state;
	}
	return state;
}

function comments(state = [], action) {
	if (typeof action.postId !== 'undefined') {
		return {
			// take current state
			...state,
			// overwrite this post with new one
			[action.postId]: postComments(state[action.postId], action)
		}
	}
	return state;
}

export default comments;

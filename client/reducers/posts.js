// pure function - do not mutate state directly
function posts(state = [], action) {
	// since all reducers run each time, return the default state if not
	// action.type === 'INCREMENT_LIKES'
	switch(action.type) {
		case 'INCREMENT_LIKES' :
			console.log('Incrementing Likes!!!');
			const index = action.index;
			// update state - return entire posts array but only update one
			return [
				...state.slice(0, index),
				{
					...state[index], likes: state[index].likes + 1
				},
				...state.slice(index + 1),
			]
		default:
			return state;
	}
}

export default posts;
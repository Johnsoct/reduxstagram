import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import store, { history } from './store';

// import CSS
import css from './styles/style.styl';

// import components
import App from './components/App';
import Single from './components/Single';
import PhotoGrid from './components/PhotoGrid';

// import sentry
import Raven from 'raven-js';
import { logException } from './data/config';
Raven.config(
	'https://2735db9934544aec9e350fdd1969f2a7@sentry.io/1106690',
	{
		tags: {
			git_commit: 'fake_git_commit',
			userLevel: 'editor'
		}
	}
).install();

const Routes = (
	<Provider store={store}>
		<Router history={history}>
			<Route path="/" component={App}>
				<IndexRoute component={PhotoGrid}></IndexRoute>
				<Route path="/view/:postId" component={Single}></Route>
			</Route>
		</Router>
	</Provider>
);

render(Routes, document.querySelector('#root')); 
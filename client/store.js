import { createStore, compse } from 'redux';
import { syncHistoryWithStore } from 'react-router-redux';
// ^ allows us to hook up our router with redux
import { browserHistory } from 'react-router';

// import root reducer
import rootReducer from './reducers/index';
// import default data to play with
import comments from './data/comments';
import posts from './data/posts';

// create object for default data
const defaultState = {
	posts,
	comments
};

const store = createStore(rootReducer, defaultState);

// sync the router history (browserHistory) with our store
// allows the store to use our history
export const history = syncHistoryWithStore(browserHistory, store);

// add hot reloading to reducers
if (module.hot) {
	module.hot.accept('./reducers/', () => {
		// swaps out modules if hot, must use require inside functions (es6 not allowed in functions)
		const nextRootReducer = require('./reducers/index').default;
		store.replaceReducer(nextRootReducer);
	});
}

export default store;